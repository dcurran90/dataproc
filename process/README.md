# Example project for data processing using Docker containers

This example project will take some data and insert it into a MondoDB instance (in this case also a container but it doesn't have to be)

There is more to this process and it should showcase some skills with Jenkins, Kubernetes, Python and MongoDB

The process is as follows:

* Jenkins picks up changes to the master git branch, pulls the code down and runs the tests
* Jenkins builds the docker image and pushes this to Docker Hub schizoid90/dataproc
* A second pipeline will be kicked off that generates the data
* Then instructs kubernetes to build a container from the afformentioned image and passes the data for processing
* The Python code then places this data into MongoDB
