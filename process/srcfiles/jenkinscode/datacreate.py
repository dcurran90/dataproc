from random import randint

class CreateData:
    def __init__(self, points):
        self.points = points
        self.data = {}
        self.fnames = ['David', 'Andrew', 'Gary', 'Holly', 'Silvia', 'Delwyn', 'Baz', 'Jerry', 'Helen', 'Lindsay']
        self.snames = ['Smith', 'Jones', 'Williams', 'Jenkins', 'Sterling', 'Edwards', 'Taylor']

    def create(self):
        self.data['data'] = {}
        for x in range(0, self.points):
            self.data['data'][x] = {'firstname': self.fnames[randint(0, (len(self.fnames)-1))],
                                    'surname': self.snames[randint(0, (len(self.snames)-1))],
                                    'age': randint(18, 65),
                                    'score': randint(0, 100)}
        return self.data
