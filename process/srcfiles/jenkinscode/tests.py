import os
from processcode.dataprocess import ProcessData
from .datacreate import CreateData


class RunTests:
    def __init__(self, points=20):
        self.fails = 0
        self.tests = {}
        self.points = points

    def testing(self):
        cd = CreateData(self.points)
        data = cd.create()
        self.tests['datatotal'] = "PASS" if len(data['data']) == self.points else "FAIL"
        pd = ProcessData(data = data)
        mongoinsert = pd.process()
        self.tests['mongoinsert'] = "PASS" if len(mongoinsert['result']) == self.points else "FAIL"
        self.tests['mongocount'] = "PASS" if mongoinsert['count'] == self.points else "FAIL"

        for i in self.tests:
            if self.tests[i] != "PASS":
                self.fails += 1
                print("Test %s failed" % i)
        del os.environ['TEST_ENV']
        
        if self.fails != 0 :
            print("Too many failures - %s" % self.fails)
            exit(1)
        else:
            print("All tests successful")
            pd.removedocuments()
            exit(0)
