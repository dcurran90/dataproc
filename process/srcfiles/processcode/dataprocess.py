import os
from pymongo import MongoClient

class ProcessData:
    def __init__(self, data=""):
        self.database = "testdata" if os.getenv('TEST_ENV') else "data"
        client = MongoClient(os.getenv('MONGO_CLIENT'), int(os.getenv('MONGO_PORT')))
        self.db = client[self.database]
        if os.getenv('MONGO_USER'):
            self.db.authenticate(os.getenv('MONGO_USER'), os.getenv('MONGO_PASS'))
        self.data = data

    def process(self):
        """
        Insert the given data to MongDB
        and keep a track of IDs and how many records have been inserted
        """
        response = {}
        response['result'] = {}
        num = 0
        for i in self.data['data']:
            num += 1
            result = self.db[self.database].insert_one(self.data['data'][i])
            response['result'][num] = {'id': result.inserted_id}
            response['count'] = num
        return response

    def removedocuments(self):
        """
        remove all documents after testing
        """
        self.db[self.database].delete_many({})


if __name__ == "__main__":
    """
    Use the data from CreateData.create() in ProcessData.process() to add to MongoDB
    """
    cd = CreateData(200)
    data = cd.create()
    pd = ProcessData(data)
    process()
