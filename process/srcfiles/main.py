import os, sys
from jenkinscode import tests, datacreate
from processcode import dataprocess

if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1] == "test":
            os.environ['TEST_ENV'] = "1"
            rt = tests.RunTests()
            rt.testing()
        else:
            print("Unrecognised argument %s" % sys.argv[1])
    else:
        cd = datacreate.CreateData(200)
        data = cd.create()
        pd = dataprocess.ProcessData(data)
        pd.process()
