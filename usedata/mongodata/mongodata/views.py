import os
import json
from pymongo import MongoClient
from django.http import JsonResponse

version = "v0.2"

def mongodata(request):
    if os.getenv('MONGO_CLIENT_FILE'):
        with open(os.getenv('MONGO_CLIENT_FILE', 'r')) as mcf:
            os.environ["MONGO_CLIENT"] = mcf.read()
        with open(os.getenv('MONGO_PORT_FILE', 'r')) as mpo:
            os.environ['MONGO_PORT'] = mpo.read()
        with open(os.getenv('MONGO_USER_FILE', 'r')) as muf:
            os.environ['MONGO_USER'] = muf.read()
        with open(os.getenv('MONGO_PASS_FILE', 'r')) as mps:
            os.environ['MONGO_PASS'] = mps.read()

    response = {}
    response['status'] = 200
    response['version'] = version
    client = MongoClient(os.getenv('MONGO_CLIENT'), int(os.getenv('MONGO_PORT')))
    db = client['data']
    db.authenticate(os.getenv('MONGO_USER'), os.getenv('MONGO_PASS'))
    collection = db['data']

    response['data'] = list(collection.find({}, {'_id': 0}))
    print(type(response))

    return JsonResponse(response)

def statusHandler(request):
    response = {}
    response['status'] = 200
    response['version'] = version
    response['message'] = "API is running"

    return JsonResponse(response)
