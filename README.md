Microservices code to deploy containers into Kubernetes.

process - contains the code for creating and processing some data

usedata - Django app that reads the data from MongoDB and returns it as JSON from the /data endpoint

Containers can be found on my dockerhub account:  
https://hub.docker.com/u/schizoid90/

This is all written up in a two part blog on medium:  
https://medium.com/@david.curran3/microservices-running-in-kubernetes-powered-by-jenkins-part-1-6c063b462ff2

Not Pictured
------------

MongoDB - basic MongoDB container with a persistent volume claim to keep the database on disk

Nginx web server - Simple web page to display the data in a pretty table. Code can be seen in this JSFiddle
